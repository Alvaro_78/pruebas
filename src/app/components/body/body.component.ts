import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent  {

  mostrar = false

  frase: any = {
    mensaje: 'Un gran poder requiere una gran responsabillidad',
    autor: 'Ben Parker'
  }

  personajes: string[] = ['Spiderman', 'Venom', 'Dr.Octopus']

  

}
